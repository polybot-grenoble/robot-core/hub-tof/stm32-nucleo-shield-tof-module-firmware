# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [0.4.0] - 2020-11-21
### Added
- CAN interface
- FreeRTOS debugging tools

## [0.3.0] - 2020-10-26
### Added
- Ping function in VL53L0X lib
### Changed
- Sensor auto-detection
- Remove static sensor config

## [0.2.1] - 2020-10-12
### Added
- Readme
- Changelog
- License


## [0.2.0] - 2020-10-10
### Added
- Add documentation for VL53L0X library
### Changed
- Convert VL53L0X library to C
- Convert the project from C++ to C.


## [0.1.0] - 2020-10-04
### Added
- Import VL53L0X library
- Create a task that initialize VL53L0X sensors and make them measure.