/**
* @file systemDriver.h
* @author Charles Blanchard
* @brief
* @version 0.1
* @date 20/12/2020
*
*/

#ifndef __SYSTEM_DRIVER_H
#define __SYSTEM_DRIVER_H

/* Includes */

#include "main.h"
#include "cmsis_os.h"

#include "systemAPI.h"

/* Prototypes */

system_t systemDriverRegisterCallbacks();


#endif /* __SYSTEM_DRIVER_H__ */
