/**
* @file systemTask.c
* @author Charles Blanchard
* @brief
* @version 0.1
* @date 13/12/2020
*
*/

/* Includes */

#include "systemDriver.h"
#include "interfaceAPI.h"
#include "sensorsAPI.h"

/* Task */

void startSystemTask(void const * argument)
{
	systemPrintHeader();

	systemLog("Init Interface Module: ...");
    interfaceAPIInit();
    systemLog("Init Interface Module: OK");

    systemLog("Init Sensors Module: ...");
    sensorsAPIInit();
    systemLog("Init Sensors Module: OK");

    systemDriverRegisterCallbacks();

	for(;;)
	{
		osDelay(1);
	}
}
