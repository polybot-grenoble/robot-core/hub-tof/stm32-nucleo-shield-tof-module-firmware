/**
* @file systemConfig.c
* @author Charles Blanchard
* @brief
* @version 0.1
* @date 17/09/2020
*
*/


/* Includes */

#include "systemConfig.h"
#include "main.h"


/* Typedefs */

typedef struct
{
	uint8_t nb_sensors;

	uint8_t address[10];
	uint8_t channel[10];
	uint8_t xshut[10];
} systemConfig_t;


/* Global variables */

static systemConfig_t config;


/* Functions */

system_t systemConfigGetNumberOfSensors(uint8_t *nb)
{
	*nb = config.nb_sensors;
	return SYSTEM_OK;
}

system_t systemConfigGetSensor(uint8_t index, uint8_t *address, uint8_t *channel, uint8_t *xSHUTIndex)
{
	if(address != NULL)
		*address = config.address[index];

	if(channel != NULL)
		*channel = config.channel[index];

	if(xSHUTIndex != NULL)
		*xSHUTIndex = config.xshut[index];

	return SYSTEM_OK;
}

system_t systemConfigAddSensor(uint8_t xSHUTIndex, uint8_t channel)
{
	config.address[config.nb_sensors] = 0x50 + (config.nb_sensors*2);
	config.channel[config.nb_sensors] = channel;
	config.xshut[config.nb_sensors] = xSHUTIndex;

	config.nb_sensors++;

	return SYSTEM_OK;
}

system_t systemConfigInit()
{
	config.nb_sensors = 0;

	// Init sensors
	for(int i=0; i<10; i++)
	{
		config.address[i] = 0;
		config.channel[i] = 0;
		config.xshut[i] = 0;
	}

	return SYSTEM_OK;
}
