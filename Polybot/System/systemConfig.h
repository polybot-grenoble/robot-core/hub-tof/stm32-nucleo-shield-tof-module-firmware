/**
* @file systemConfig.h
* @author Charles Blanchard
* @brief
* @version 0.1
* @date 17/09/2020
*
*/

#ifndef __SYSTEM_CONFIG_H__
#define __SYSTEM_CONFIG_H__


/* Includes */

#include "systemAPI.h"


/* Prototypes */

system_t systemConfigInit();

system_t systemConfigAddSensor(uint8_t xSHUTIndex, uint8_t channel);

system_t systemConfigGetNumberOfSensors(uint8_t *nb);
system_t systemConfigGetSensor(uint8_t index, uint8_t *address, uint8_t *channel, uint8_t *xSHUTIndex);


#endif
