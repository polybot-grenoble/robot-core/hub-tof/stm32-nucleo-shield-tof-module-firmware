/**
* @file systemAPI.c
* @author Charles Blanchard
* @brief
* @version 0.1
* @date 17/09/2020
*
*/


/* Includes */

#include "systemAPI.h"

#include <stdio.h>
#include <stdarg.h>
#include <string.h>

#include "systemConfig.h"


/* Extern variables */

extern UART_HandleTypeDef huart2;
extern osMutexId systemLogMutexHandle;

/* Functions */

static char dateBuffer[18];
static char msgBuffer[1024];

static system_t systemPrint(char *str)
{
	HAL_UART_Transmit(&huart2, (uint8_t *)str, strlen(str), HAL_MAX_DELAY);
	return SYSTEM_OK;
}

system_t systemPrintHeader()
{
	uint8_t len;

	systemPrint("\n\n\r");
	systemPrint("======================================================================\r\n");
	systemPrint("=                     (C) COPYRIGHT 2020 Polybot                     =\r\n");
	systemPrint("=                                                                    =\r\n");
	systemPrint("=                           HUB ToF Module                           =\r\n");
	systemPrint("=                                                                    =\r\n");

	snprintf(dateBuffer, 16, "FW V%s", FW_VERSION);
	len = strlen(dateBuffer);
	snprintf(msgBuffer, 128, "=%*s%s%*s=\r\n",(68-len)/2, "", dateBuffer, (68-len)/2 + len%2, "");
	systemPrint(msgBuffer);

	snprintf(dateBuffer, 16, "HW V%s", HW_VERSION);
	len = strlen(dateBuffer);
	snprintf(msgBuffer, 128, "=%*s%s%*s=\r\n",(68-len)/2, "", dateBuffer, (68-len)/2 + len%2, "");
	systemPrint(msgBuffer);

	systemPrint("======================================================================\r\n");
	return SYSTEM_OK;
}

system_t systemLog(const char *format, ...)
{
	va_list args;
	va_start(args, format);

	osMutexWait(systemLogMutexHandle, osWaitForever);

	vsnprintf(msgBuffer, 64, format, args);

	uint32_t tick = osKernelSysTick();

	uint8_t hr = tick / 3600000; //3600000 milliseconds in an hour
	tick -= 3600000 * hr;

	uint8_t min = tick / 60000; //60000 milliseconds in a minute
	tick -= 60000 * min;

	uint8_t sec = tick / 1000; //1000 milliseconds in a second
	tick -= 1000 * sec;

	snprintf(dateBuffer, 18, "\r\n[%02i:%02i:%02i:%03lu] ", hr, min, sec, tick);

	systemPrint(dateBuffer);
	systemPrint(msgBuffer);

	osMutexRelease(systemLogMutexHandle);

	return SYSTEM_OK;
}



system_t systemAPIConfigInit()
{
	return systemConfigInit();
}

system_t systemAPIConfigAddSensor(uint8_t xSHUTIndex, uint8_t channel)
{
	return systemConfigAddSensor(xSHUTIndex, channel);
}

system_t systemAPIGetNumberOfSensors(uint8_t *nb)
{
	return systemConfigGetNumberOfSensors(nb);
}

system_t systemAPIGetSensor(uint8_t index, uint8_t *address, uint8_t *channel, uint8_t *xSHUTIndex)
{
	return systemConfigGetSensor(index, address, channel, xSHUTIndex);
}
