/**
* @file systemDriver.c
* @author Charles Blanchard
* @brief
* @version 0.1
* @date 20/12/2020
*
*/

/* Includes */

#include "systemDriver.h"
#include "interfaceAPI.h"
#include "sensorsAPI.h"

#include "systemConfig.h"

#include "string.h"


/* Functions */

system_t systemDriverInit()
{

	return SYSTEM_OK;
}


static system_t systemDriverCommon(interfaceMessageId_t id, uint8_t *buffer, uint8_t size)
{
	switch(id)
	{
	case TOF_MODULE_CMD_RESET:
		HAL_NVIC_SystemReset();
		break;

	case TOF_MODULE_GET_HW_VERSION:
		strlcpy((char *)buffer, HW_VERSION, 8);
		interfaceAPISend(TOF_MODULE_GET_HW_VERSION, buffer, strlen((char *)buffer));
		break;

	case TOF_MODULE_GET_FW_VERSION:
		strlcpy((char *)buffer, FW_VERSION, 8);
		interfaceAPISend(TOF_MODULE_GET_FW_VERSION, buffer, strlen((char *)buffer));
		break;

	default:
		return SYSTEM_ERROR;
	}

	return SYSTEM_OK;
}

static system_t systemDriverGetSensors(interfaceMessageId_t id, uint8_t *buffer, uint8_t size)
{
	switch(id)
	{
	case TOF_MODULE_GET_STATE:
		sensorsAPIGetSensorsState(&buffer[0]);
		interfaceAPISend(TOF_MODULE_GET_STATE, &buffer[0], 1);
		break;

	case TOF_MODULE_GET_NB_SENSORS:
		systemConfigGetNumberOfSensors(&buffer[0]);
		interfaceAPISend(TOF_MODULE_GET_NB_SENSORS, &buffer[0], 1);
		break;

	default:
			return SYSTEM_ERROR;
	}

	return SYSTEM_OK;
}

system_t systemDriverRegisterCallbacks()
{
	uint8_t status = SYSTEM_OK;


	status |= interfaceAPIRegisterCallback(TOF_MODULE_CMD_RESET, 		&systemDriverCommon);
	status |= interfaceAPIRegisterCallback(TOF_MODULE_GET_STATE, 		&systemDriverGetSensors);
	status |= interfaceAPIRegisterCallback(TOF_MODULE_GET_NB_SENSORS, 	&systemDriverGetSensors);
	status |= interfaceAPIRegisterCallback(TOF_MODULE_GET_HW_VERSION, 	&systemDriverCommon);
	status |= interfaceAPIRegisterCallback(TOF_MODULE_GET_FW_VERSION, 	&systemDriverCommon);

	return status;
}

