/**
* @file systemAPI.h
* @author Charles Blanchard
* @brief
* @version 0.1
* @date 17/09/2020
*
*/

#ifndef __SYSTEM_API_H__
#define __SYSTEM_API_H__


/* Includes */

#include "main.h"
#include "cmsis_os.h"


/* Defines */

#define HW_VERSION	"1.0"
#define FW_VERSION	"0.5.0"

#define NACK	0
#define ACK		1
#define BUSY	2

/* Typedefs */

typedef enum
{
	SYSTEM_OK,
	SYSTEM_ERROR
} system_t;


/* Prototypes */

system_t systemPrintHeader();
system_t systemLog(const char *format, ...);

system_t systemAPIConfigInit();

system_t systemAPIConfigAddSensor(uint8_t xSHUTIndex, uint8_t channel);

system_t systemAPIGetNumberOfSensors(uint8_t *nb);
system_t systemAPIGetSensor(uint8_t index, uint8_t *address, uint8_t *channel, uint8_t *xSHUTIndex);

#endif
