/**
* @file sensorsDriver.c
* @author Charles Blanchard
* @brief
* @version 0.1
* @date 17/09/2020
*
*/

/* Includes */

#include "sensorsDriver.h"

#include "systemAPI.h"
#include "interfaceAPI.h"

#include "FreeRTOS.h"
#include "portable.h"


/* Extern variables */

extern I2C_HandleTypeDef hi2c1;
extern I2C_HandleTypeDef hi2c3;


extern sensorsHandler_t *sensorsHandler;


/* Functions */

system_t sensorsDriverSend(uint32_t cmd, uint8_t value)
{
	return interfaceAPISend(cmd, &value, 1);
}


static system_t sensorsReceive(interfaceMessageId_t id, uint8_t *buffer, uint8_t size)
{
	uint32_t message = id;
	xQueueSend(sensorsHandler->queue, &message, 0);
	return SYSTEM_OK;
}


system_t sensorsDriverInit(sensorsHandler_t *handler)
{
	uint8_t status = SYSTEM_OK;

	handler->state = WAITING;

	handler->queue = xQueueCreate( 8, sizeof( uint32_t ) );
	if(handler->queue == NULL)
	{
		return SYSTEM_ERROR;
	}

	status |= interfaceAPIRegisterCallback(TOF_MODULE_CMD_INIT, 		&sensorsReceive);
	status |= interfaceAPIRegisterCallback(TOF_MODULE_CMD_START, 		&sensorsReceive);
	status |= interfaceAPIRegisterCallback(TOF_MODULE_CMD_STOP, 		&sensorsReceive);
	status |= interfaceAPIRegisterCallback(TOF_MODULE_GET_STATE, 		&sensorsReceive);
	status |= interfaceAPIRegisterCallback(TOF_MODULE_GET_NB_SENSORS, 	&sensorsReceive);

	return status;
}

sensorsState_t sensorsDriverGetState(sensorsHandler_t *sensorsHandler)
{
	return sensorsHandler->state;
}

system_t sensorsDriverSetState(sensorsHandler_t *sensorsHandler, sensorsState_t newState)
{
	sensorsHandler->state = newState;
	return SYSTEM_OK;
}



system_t sensorsDriverHandlerInit(sensor_t *sensor, uint8_t i2cAddress, uint8_t i2cChannel, uint8_t xshutIndex)
{
	sensor->hsensor = (VL53L0X_t*) pvPortMalloc(sizeof(VL53L0X_t));

	sensor->address = i2cAddress;
	sensor->hi2c = (i2cChannel == 1) ? &hi2c1 : &hi2c3;
	sensor->XSHUT_Index = xshutIndex;

	return SYSTEM_OK;
}


system_t sensorsDriverHandlerDeInit(sensor_t *sensor)
{
	vPortFree(sensor->hsensor);

	return SYSTEM_OK;
}

typedef struct
{
	GPIO_TypeDef* GPIOx;
	uint16_t GPIO_Pin;
} XSHUT_t;


/* Global variables */

const XSHUT_t XSHUT[] =
{
		{ XSHUT_0_GPIO_Port, XSHUT_0_Pin },
		{ XSHUT_1_GPIO_Port, XSHUT_1_Pin },
		{ XSHUT_2_GPIO_Port, XSHUT_2_Pin },
		{ XSHUT_3_GPIO_Port, XSHUT_3_Pin },
		{ XSHUT_4_GPIO_Port, XSHUT_4_Pin },
		{ XSHUT_5_GPIO_Port, XSHUT_5_Pin },
		{ XSHUT_6_GPIO_Port, XSHUT_6_Pin },
		{ XSHUT_7_GPIO_Port, XSHUT_7_Pin },
		{ XSHUT_8_GPIO_Port, XSHUT_8_Pin },
		{ XSHUT_9_GPIO_Port, XSHUT_9_Pin },
};


system_t sensorsDriverShutdownSensor(uint8_t index)
{
	HAL_GPIO_WritePin(XSHUT[index].GPIOx, XSHUT[index].GPIO_Pin, GPIO_PIN_RESET);
	return SYSTEM_OK;
}


system_t sensorsDriverPowerUpSensor(uint8_t index)
{
	HAL_GPIO_WritePin(XSHUT[index].GPIOx, XSHUT[index].GPIO_Pin, GPIO_PIN_SET);
	return SYSTEM_OK;
}

/* Functions */

system_t sensorsDriverInitSensors(sensorsHandler_t *sensorsHandler)
{
	uint8_t nbSensors = 0;

	sensor_t scanner;
	uint8_t i2cAddress, i2cChannel, xshutIndex;

	/* ----------- SCANNING POTENTIAL SENSORS ------------- */

	// Create empty config
	systemAPIConfigInit();

	// Shutdown all potential sensors by putting all xshut pin to 0
	for(xshutIndex=0; xshutIndex<10; xshutIndex++)
	{
		sensorsDriverShutdownSensor(xshutIndex);
	}


	// Try to communicate with sensor one by one
	for(xshutIndex=0; xshutIndex<10; xshutIndex++)
	{
		for(i2cChannel=1; i2cChannel<4; i2cChannel=i2cChannel+2)
		{
			sensorsDriverHandlerInit(&scanner, 80, i2cChannel, xshutIndex);

			sensorsDriverPowerUpSensor(xshutIndex);

			osDelay(10);

			VL53L0X_init(scanner.hsensor, scanner.hi2c, 1);

			if(VL53L0X_ping(scanner.hsensor))
			{
				systemLog("Sensor %i on I2C channel %i", xshutIndex, i2cChannel);
				systemAPIConfigAddSensor(xshutIndex, i2cChannel);
			}

			sensorsDriverHandlerDeInit(&scanner);

			sensorsDriverShutdownSensor(xshutIndex);

			osDelay(10);
		}
	}



	/* ----------- CONFIGURING HANDLERS ------------- */

	// Get number of sensors
	systemAPIGetNumberOfSensors(&nbSensors);

	if(nbSensors == 0)
	{
		return SYSTEM_ERROR;
	}

	// Allocate memory for sensor handlers
	sensorsHandler->sensors = (sensor_t *) pvPortMalloc(nbSensors * sizeof(sensor_t));

	if(sensorsHandler->sensors == NULL)
	{
		return SYSTEM_ERROR;
	}

	// Initialize sensors handler
	for(int i=0; i<nbSensors; i++)
	{
		systemAPIGetSensor(i, &i2cAddress, &i2cChannel, &xshutIndex);

		sensorsDriverHandlerInit(&sensorsHandler->sensors[i],i2cAddress, i2cChannel, xshutIndex);

		systemLog("Sensor %i: Config Get", xshutIndex);
	}



	/* ----------- INITIALIZING SENSORS ------------- */

	// Shutdown all sensors
	for(xshutIndex=0; xshutIndex<nbSensors; xshutIndex++)
	{
		sensorsDriverShutdownSensor(sensorsHandler->sensors[xshutIndex].XSHUT_Index);
		osDelay(10);
	}

	// Init sensors one by one
	for(int i=0; i<nbSensors; i++)
	{
		sensorsDriverPowerUpSensor(sensorsHandler->sensors[i].XSHUT_Index);

		osDelay(10);

		VL53L0X_init(sensorsHandler->sensors[i].hsensor, sensorsHandler->sensors[i].hi2c, 1);

		osDelay(10);

		VL53L0X_setAddress(sensorsHandler->sensors[i].hsensor, sensorsHandler->sensors[i].address);

		osDelay(10);

		systemLog("Sensor %i: Config Set (address 0x%x)", i, sensorsHandler->sensors[i].address);
	}

	return SYSTEM_OK;
}


system_t sensorsDriverStartMeasuring(sensorsHandler_t *sensorsHandler)
{
	uint8_t nbSensors = 0;
	systemAPIGetNumberOfSensors(&nbSensors);

	for(int i=0; i<nbSensors; i++)
	{
		VL53L0X_startContinuous(sensorsHandler->sensors[i].hsensor, 0);
		systemLog("Sensor %i: Measure Started", i);
		osDelay(10);
	}

	return SYSTEM_OK;
}


system_t sensorsDriverStopMeasuring(sensorsHandler_t *sensorsHandler)
{
	uint8_t nbSensors = 0;
	systemAPIGetNumberOfSensors(&nbSensors);

	for(int i=0; i<nbSensors; i++)
	{
		VL53L0X_stopContinuous(sensorsHandler->sensors[i].hsensor);
		systemLog("Sensor %i: Measure Stopped", i);

		osDelay(10);
	}

	return SYSTEM_OK;
}


static system_t sensorsDriverSendMeasures(uint16_t *measures, uint8_t nbSensors)
{
	uint8_t buffer[8];

	buffer[0] = 0; // START FRAME
	interfaceAPISend(TOF_MODULE_MEASURE, buffer, 1);

	buffer[0] = 1; // VALUE FRAME
	for(int i=0; i<nbSensors; i++)
	{
		buffer[1] = i;
		buffer[2] = measures[i] >> 8;
		buffer[3] = measures[i] & 0xFF;
		interfaceAPISend(TOF_MODULE_MEASURE, buffer, 4);
	}

	buffer[0] = 2; // STOP FRAME
	interfaceAPISend(TOF_MODULE_MEASURE, buffer, 1);

	return SYSTEM_OK;
}

system_t sensorsDriverMeasure(sensorsHandler_t *sensorsHandler)
{
	uint8_t nbSensors = 0;
	uint16_t measures[10];

	systemAPIGetNumberOfSensors(&nbSensors);

	for(int i=0; i<nbSensors; i++)
	{
		measures[i] = VL53L0X_readRangeContinuousMillimeters(sensorsHandler->sensors[i].hsensor);
		systemLog("Sensor %i: Measure Get (%imm)", i, measures[i]);
	}

	sensorsDriverSendMeasures(measures, nbSensors);

	return SYSTEM_OK;
}

