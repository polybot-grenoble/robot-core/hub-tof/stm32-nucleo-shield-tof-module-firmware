/**
* @file sensorsDriver.h
* @author Charles Blanchard
* @brief
* @version 0.1
* @date 17/09/2020
*
*/

#ifndef __SENSORS_DRIVER_H__
#define __SENSORS_DRIVER_H__

/* Includes */

#include "systemAPI.h"
#include "VL53L0X.h"


/* Typedefs */

typedef struct
{
	VL53L0X_t *hsensor;
	uint8_t address;
	I2C_HandleTypeDef *hi2c;
	uint8_t XSHUT_Index;
} sensor_t;


typedef enum
{
	FAILURE = 0,
	WAITING,
	INITIALIZING,
	INITIALIZED,
	STARTING,
	MEASURING,
	STOPPING
} sensorsState_t;


typedef struct
{
	sensorsState_t state;
	QueueHandle_t queue;
	sensor_t *sensors;
} sensorsHandler_t;

/* Prototypes */

system_t sensorsDriverInit(sensorsHandler_t *sensorsHandler);

system_t sensorsDriverSend(uint32_t cmd, uint8_t value);

system_t sensorsDriverInitSensors(sensorsHandler_t *sensorsHandler);
system_t sensorsDriverStartMeasuring(sensorsHandler_t *sensorsHandler);
system_t sensorsDriverStopMeasuring(sensorsHandler_t *sensorsHandler);
system_t sensorsDriverMeasure(sensorsHandler_t *sensorsHandler);

sensorsState_t sensorsDriverGetState(sensorsHandler_t *sensorsHandler);
system_t sensorsDriverSetState(sensorsHandler_t *sensorsHandler, sensorsState_t newState);

#endif
