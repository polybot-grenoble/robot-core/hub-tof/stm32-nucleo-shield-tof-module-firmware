/**
* @file sensorsAPI.h
* @author Charles Blanchard
* @brief
* @version 0.1
* @date 13/12/2020
*
*/

#ifndef __SENSORS_API_H__
#define __SENSORS_API_H__


/* Includes */

#include "main.h"
#include "cmsis_os.h"

#include "systemAPI.h"


/* Prototypes */

system_t sensorsAPIInit();

system_t sensorsAPIGetSensorsState(uint8_t *state);

#endif /* __SENSORS_API_H */
