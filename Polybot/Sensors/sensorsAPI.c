/**
* @file sensorsAPI.c
* @author Charles Blanchard
* @brief
* @version 0.1
* @date 13/02/2020
*
*/

/* Includes */

#include "sensorsAPI.h"
#include "sensorsDriver.h"


/* Variables */

extern sensorsHandler_t *sensorsHandler;

extern osSemaphoreId sensorsInitCallHandle;
extern osSemaphoreId sensorsInitReturnHandle;


/* Functions */

system_t sensorsAPIInit()
{
	// Unlock interface task to launch its initialization
	osSemaphoreRelease(sensorsInitCallHandle);

	// Wait for interfaceTask to finished its initialization
	osSemaphoreWait(sensorsInitReturnHandle, osWaitForever);

	return SYSTEM_OK;
}

system_t sensorsAPIGetSensorsState(uint8_t *state)
{
	*state = sensorsDriverGetState(sensorsHandler);
	return SYSTEM_OK;
}
