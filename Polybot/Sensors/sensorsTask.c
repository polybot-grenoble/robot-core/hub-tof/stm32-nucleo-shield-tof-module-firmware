/**
 * @file sensorsTask.c
 * @author Charles Blanchard
 * @brief
 * @version 0.1
 * @date 11/09/2020
 *
 */


/* Includes */

#include "systemAPI.h"
#include "interfaceAPI.h"

#include "sensorsDriver.h"

#include "limits.h"


/* Global variables */

sensorsHandler_t *sensorsHandler;

extern osSemaphoreId sensorsInitCallHandle;
extern osSemaphoreId sensorsInitReturnHandle;


/* Task */


void startSensorsTask(void const * argument)
{
	uint32_t message;

	// Wait for signal to start
	osSemaphoreWait(sensorsInitCallHandle, osWaitForever);

	// Instanciate handler
	sensorsHandler = (sensorsHandler_t*) pvPortMalloc(sizeof(sensorsHandler_t));

	// Init sensors module
	sensorsDriverInit(sensorsHandler);

	// Notify that init is done
	osSemaphoreRelease(sensorsInitReturnHandle);


	while(1)
	{
		if(xQueueReceive(sensorsHandler->queue, &message, 0) == pdTRUE)
		{
			switch(message)
			{
			case TOF_MODULE_CMD_INIT:
				if(sensorsDriverGetState(sensorsHandler) ==  WAITING || sensorsDriverGetState(sensorsHandler) == FAILURE)
				{
					sensorsDriverSend(TOF_MODULE_CMD_INIT, BUSY);
					sensorsDriverSetState(sensorsHandler, INITIALIZING);
				}
				else
				{
					sensorsDriverSend(TOF_MODULE_CMD_INIT, NACK);
				}
				break;

			case TOF_MODULE_CMD_START:
				if(sensorsDriverGetState(sensorsHandler) ==  INITIALIZED)
				{
					sensorsDriverSend(TOF_MODULE_CMD_START, BUSY);
					sensorsDriverSetState(sensorsHandler, STARTING);
				}
				else
				{
					sensorsDriverSend(TOF_MODULE_CMD_START, NACK);
				}
				break;

			case TOF_MODULE_CMD_STOP:
				if(sensorsDriverGetState(sensorsHandler) ==  MEASURING)
				{
					sensorsDriverSend(TOF_MODULE_CMD_STOP, BUSY);
					sensorsDriverSetState(sensorsHandler, STOPPING);
				}
				else
				{
					sensorsDriverSend(TOF_MODULE_CMD_STOP, NACK);
				}
				break;
			}
		}


		switch(sensorsDriverGetState(sensorsHandler))
		{
		case WAITING:
			osDelay(1);
			break;

		case INITIALIZING:
			if(sensorsDriverInitSensors(sensorsHandler) == SYSTEM_OK)
			{
				sensorsDriverSetState(sensorsHandler, INITIALIZED);
				sensorsDriverSend(TOF_MODULE_CMD_INIT, ACK);
			}
			else
			{
				sensorsDriverSetState(sensorsHandler, FAILURE);
				sensorsDriverSend(TOF_MODULE_CMD_INIT, NACK);
			}
			break;

		case INITIALIZED:
			osDelay(1);
			break;

		case STARTING:
			sensorsDriverStartMeasuring(sensorsHandler);
			sensorsDriverSend(TOF_MODULE_CMD_START, ACK);
			sensorsDriverSetState(sensorsHandler, MEASURING);
			break;


		case MEASURING:
			sensorsDriverMeasure(sensorsHandler);
			osDelay(100);
			break;


		case STOPPING:
			sensorsDriverStopMeasuring(sensorsHandler);
			sensorsDriverSend(TOF_MODULE_CMD_STOP, ACK);
			sensorsDriverSetState(sensorsHandler, INITIALIZED);
			break;

		case FAILURE:
			osDelay(1);
			break;
		}
	}
}
