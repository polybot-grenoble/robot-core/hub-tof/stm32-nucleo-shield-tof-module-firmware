/**
* @file interfaceAPI.h
* @author Charles Blanchard
* @brief
* @version 0.2
* @date 30/11/2020
*
*/

#ifndef __INTERFACE_API_H__
#define __INTERFACE_API_H__


/* Includes */

#include "main.h"
#include "cmsis_os.h"

#include "interfaceMessageId.h"
#include "interfaceDriver.h"

/* Prototypes */

/**
* Initialize the CAN interface module.
* @return SYSTEM_OK if everything goes well, SYSTEM_ERROR otherwise.
*/
system_t interfaceAPIInit();


/**
* Register a callback to be called when a given message id is received.
* @param id The id of the message.
* @param callback The callback to call.
* @return SYSTEM_OK if everything goes well, SYSTEM_ERROR otherwise.
*/
system_t interfaceAPIRegisterCallback(interfaceMessageId_t id, interfaceDriverCallback_t callback);

/**
* Unregister the callback mapped to a given message id.
* @param id The id of the message.
* @return SYSTEM_OK if everything goes well, SYSTEM_ERROR otherwise.
*/
system_t interfaceAPIUnregisterCallback(interfaceMessageId_t id);

/**
* Send a message of given id on the CAN interface
* @param id The id of the message.
* @param buffer Data to send.
* @param size Size of the buffer.
* @return SYSTEM_OK if everything goes well, SYSTEM_ERROR otherwise.
*/
system_t interfaceAPISend(interfaceMessageId_t id, uint8_t *buffer, uint8_t size);


#endif /* __INTERFACE_API_H */
