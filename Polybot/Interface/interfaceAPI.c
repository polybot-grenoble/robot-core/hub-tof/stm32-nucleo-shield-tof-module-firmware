/**
* @file interfaceAPI.c
* @author Charles Blanchard
* @brief
* @version 0.2
* @date 30/11/2020
*
*/

/* Includes */

#include "interfaceAPI.h"



/* Variables */

extern interfaceHandler_t *interfaceHandler;
extern osSemaphoreId interfaceInitCallHandle;
extern osSemaphoreId interfaceInitReturnHandle;


/* Functions */

/**
* Initialize the CAN interface module.
* @return SYSTEM_OK if everything goes well, SYSTEM_ERROR otherwise.
*/
system_t interfaceAPIInit()
{
	// Unlock interface task to launch its initialization
	osSemaphoreRelease(interfaceInitCallHandle);

	// Wait for interfaceTask to finished its initialization
	osSemaphoreWait(interfaceInitReturnHandle, osWaitForever);

	return SYSTEM_OK;
}


/**
* Register a callback to be called when a given message id is received.
* @param id The id of the message.
* @param callback The callback to call.
* @return SYSTEM_OK if everything goes well, SYSTEM_ERROR otherwise.
*/
system_t interfaceAPIRegisterCallback(interfaceMessageId_t id, interfaceDriverCallback_t callback)
{
	return interfaceDriverRegisterCallback(interfaceHandler, id, callback);
}

/**
* Unregister the callback mapped to a given message id.
* @param id The id of the message.
* @return SYSTEM_OK if everything goes well, SYSTEM_ERROR otherwise.
*/
system_t interfaceAPIUnregisterCallback(interfaceMessageId_t id)
{
	return interfaceDriverUnregisterCallback(interfaceHandler, id);
}

/**
* Send a message of given id on the CAN interface
* @param id The id of the message.
* @param buffer Data to send.
* @param size Size of the buffer.
* @return SYSTEM_OK if everything goes well, SYSTEM_ERROR otherwise.
*/
system_t interfaceAPISend(interfaceMessageId_t id, uint8_t *buffer, uint8_t size)
{
	return interfaceDriverSendQuery(interfaceHandler, id, buffer, size);
}
