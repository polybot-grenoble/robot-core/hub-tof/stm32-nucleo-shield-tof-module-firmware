/**
* @file interfaceTask.c
* @author Charles Blanchard
* @brief
* @version 0.2
* @date 10/11/2020
*
*/

/* Includes */

#include "interfaceDriver.h"

#include "systemAPI.h"

#include "FreeRTOS.h"
#include "portable.h"

#include <string.h>


/* Explication :

Brief:
After its initialization, interfaceTask is waiting indefinitelly for message to come in a queue.

When a message arrived, depending on one of its attributes (interfaceMessage_t is a struct),
the task know if the message is coming from the CAN interface (reception) or if it is needed to
send it (transmission)


How to use this module ?
-> go to interfaceAPI.h and you will find all the functions needed to interract with this module.

If you need help, see how hubToF module works.


How this module is structured?
-> interfaceTask.c (this file) you will find the behaviour of the interface: what is actually done
-> interfaceAPI.h/.c: functions to interract with the module (init, send, receive etc)
-> interfaceDriver.h/.c: all the behaviour is situated here, other part of this module call this file to do their job
-> interfaceMessageId.c/.h: hold the known message id and add a way to map them to an index

How to modify this module ?
-> read all files until you understand every single step, otherwise you won't be able to do good job.
	Critical point are
		- callback management
		- initialization synchronization
		- message id and id index representation

*/

extern osSemaphoreId interfaceInitCallHandle;
extern osSemaphoreId interfaceInitReturnHandle;

interfaceHandler_t *interfaceHandler;

/**
*  Task responsible for the CAN interface.
*/
void startInterfaceTask(void const * argument)
{
	interfaceMessage_t message; //message containing data to send or received data

	// Wait for signal to start
	osSemaphoreWait(interfaceInitCallHandle, osWaitForever);

	// Instanciate handler
	interfaceHandler = (interfaceHandler_t*) pvPortMalloc(sizeof(interfaceHandler_t));

	// Init interface
	interfaceDriverInit(interfaceHandler);

	// Notify that init is done
	osSemaphoreRelease(interfaceInitReturnHandle);

	for(;;)
	{
		// Waits indefinitely for a message to be received
		interfaceDriverWaitForMessage(interfaceHandler, &message);

		// Check the type of message
		switch(message.type)
		{
		case RECEIVED:
			// Message received (come from RX interruption), lets process it
			interfaceDriverProcess(interfaceHandler, message.RxHeader, message.RxData);
			break;
		case TO_SEND:
			// Message to be sent (come from other part of this program), lets send it
			interfaceDriverSend(interfaceHandler, message.id, message.buffer, message.size);
			break;
		}
	}
}
