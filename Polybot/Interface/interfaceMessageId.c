/**
* @file interfaceDriver.c
* @author Charles Blanchard
* @brief
* @version 0.2
* @date 30/11/2020
*
*/

/* Includes */

#include "interfaceMessageId.h"


/* Variables */

static interfaceMessageId_t interfaceMessageMapping[] =
{
		TOF_MODULE_MEASURE,
		TOF_MODULE_CMD_RESET,
		TOF_MODULE_CMD_INIT,
		TOF_MODULE_CMD_START,
		TOF_MODULE_CMD_STOP,
		TOF_MODULE_GET_STATE,
		TOF_MODULE_GET_NB_SENSORS,
		TOF_MODULE_GET_HW_VERSION,
		TOF_MODULE_GET_FW_VERSION
};


/* Functions */

uint16_t interfaceMessageGetNbId()
{
	return sizeof(interfaceMessageMapping)/sizeof(interfaceMessageMapping[0]);
}

system_t interfaceMessageGetIndex(interfaceMessageId_t id, uint32_t *index)
{
	for(int i=0; i<interfaceMessageGetNbId(); i++)
	{
		if(interfaceMessageMapping[i] == id)
		{
			*index = i;
			return SYSTEM_OK;
		}
	}
	return SYSTEM_ERROR;
}
