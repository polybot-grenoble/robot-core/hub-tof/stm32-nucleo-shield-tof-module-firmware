/**
* @file interfaceMessageId.h
* @author Charles Blanchard
* @brief
* @version 0.2
* @date 30/11/2020
*
*/

#ifndef __INTERFACE_MESSAGE_ID_H__
#define __INTERFACE_MESSAGE_ID_H__


/* Includes */

#include "systemAPI.h"


/* Typedefs */

typedef enum
{
	TOF_MODULE_MEASURE			= 0x420,
	TOF_MODULE_CMD_RESET 		= 0x421,
	TOF_MODULE_CMD_INIT 		= 0x422,
	TOF_MODULE_CMD_START 		= 0x423,
	TOF_MODULE_CMD_STOP 		= 0x424,
	TOF_MODULE_GET_STATE 		= 0x425,
	TOF_MODULE_GET_NB_SENSORS 	= 0x426,
	TOF_MODULE_GET_HW_VERSION	= 0x427,
	TOF_MODULE_GET_FW_VERSION	= 0x428
} interfaceMessageId_t;


/* Prototypes */

uint16_t interfaceMessageGetNbId();
system_t interfaceMessageGetIndex(interfaceMessageId_t id, uint32_t *index);


#endif /* __INTERFACE_MESSAGE_ID_H__ */
