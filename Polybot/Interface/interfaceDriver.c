/**
* @file interfaceDriver.c
* @author Charles Blanchard
* @brief
* @version 0.2
* @date 30/11/2020
*
*/


/* Includes */

#include "interfaceDriver.h"

#include <string.h>


/* Global variables */

CAN_TxHeaderTypeDef   TxHeader;
CAN_RxHeaderTypeDef   RxHeader;
uint8_t               TxData[8];
uint8_t               RxData[8];
uint32_t              TxMailbox;


extern osThreadId interfaceTaskHandle;
extern interfaceHandler_t *interfaceHandler;
extern CAN_HandleTypeDef hcan1;


/* Functions */

static system_t interfaceDefaultCallback(interfaceMessageId_t id, uint8_t *buffer, uint8_t size)
{
	return SYSTEM_ERROR;
}

system_t interfaceDriverInit(interfaceHandler_t *handler)
{
	// Initialize message queue
	handler->queue = xQueueCreate( 8, sizeof( interfaceMessage_t ) );
	if(handler->queue == NULL)
	{
		return SYSTEM_ERROR;
	}

	// Alloc mem for callbacks
	handler->callback = (interfaceDriverCallback_t*) pvPortMalloc(interfaceMessageGetNbId() * sizeof(interfaceDriverCallback_t));

	// Initialize callbacks for all id to default callback
	for(int i=0; i < interfaceMessageGetNbId() ; i++)
		handler->callback[i] = &interfaceDefaultCallback;



	// Initialize CAN peripheral

	handler->hcan = &hcan1;

	CAN_FilterTypeDef  sFilterConfig;

	sFilterConfig.FilterBank = 0;
	sFilterConfig.FilterMode = CAN_FILTERMODE_IDMASK;
	sFilterConfig.FilterScale = CAN_FILTERSCALE_32BIT;
	sFilterConfig.FilterIdHigh = 0x0000;
	sFilterConfig.FilterIdLow = 0x0000;
	sFilterConfig.FilterMaskIdHigh = 0x0000;
	sFilterConfig.FilterMaskIdLow = 0x0000;
	sFilterConfig.FilterFIFOAssignment = CAN_RX_FIFO0;
	sFilterConfig.FilterActivation = ENABLE;
	sFilterConfig.SlaveStartFilterBank = 14;

	HAL_CAN_ConfigFilter(handler->hcan, &sFilterConfig);

	HAL_CAN_Start(handler->hcan);

	HAL_CAN_ActivateNotification(handler->hcan, CAN_IT_RX_FIFO0_MSG_PENDING);

	TxHeader.StdId = 0x320;
	TxHeader.ExtId = 0x01;
	TxHeader.RTR = CAN_RTR_DATA;
	TxHeader.IDE = CAN_ID_STD;
	TxHeader.DLC = 2;
	TxHeader.TransmitGlobalTime = DISABLE;

	return SYSTEM_OK;
}


system_t interfaceDriverWaitForMessage(interfaceHandler_t *handler, interfaceMessage_t *message)
{
	xQueueReceive(handler->queue , message, portMAX_DELAY);
	return SYSTEM_OK;
}

system_t interfaceDriverSend(interfaceHandler_t *handler, interfaceMessageId_t id, uint8_t *buffer, uint8_t size)
{
	if(size > 8)
	{
		return SYSTEM_ERROR;
	}

    TxHeader.StdId = id;
    TxHeader.DLC = size;

    for(int i = 0; i < size; i++)
    {
    	TxData[i] = buffer[i];
    }

    HAL_CAN_AddTxMessage(handler->hcan, &TxHeader, TxData, &TxMailbox);

    return SYSTEM_OK;
}

system_t interfaceDriverProcess(interfaceHandler_t *handler, CAN_RxHeaderTypeDef RxHeader, uint8_t *RxData)
{
	uint32_t index;

	if(interfaceMessageGetIndex(RxHeader.StdId, &index) == SYSTEM_OK)
	{
		handler->callback[index](RxHeader.StdId, RxData, RxHeader.DLC);
		return SYSTEM_OK;
	}
	else
	{
		return SYSTEM_ERROR;
	}
}



/* Callbacks related functions */

system_t interfaceDriverRegisterCallback(interfaceHandler_t *handler, interfaceMessageId_t id, interfaceDriverCallback_t callback)
{
	uint32_t index;

	// Get the index associated to the desired id
	if(interfaceMessageGetIndex(id,&index) == SYSTEM_OK)
	{
		// set the new callback
		handler->callback[index] = callback;
		return SYSTEM_OK;
	}
	else
	{
		return SYSTEM_ERROR;
	}
}

system_t interfaceDriverUnregisterCallback(interfaceHandler_t *handler, interfaceMessageId_t id)
{
	uint32_t index;

	// Get the index associated to the desired id
	if(interfaceMessageGetIndex(id,&index) == SYSTEM_OK)
	{
		// set callback to default
		handler->callback[index] = &interfaceDefaultCallback;
		return SYSTEM_OK;
	}
	else
	{
		return SYSTEM_ERROR;
	}
}

/* CAN emission and reception management related functions */

/**
* Interruption called when a message is received on the CAN bus.
*/
void HAL_CAN_RxFifo0MsgPendingCallback(CAN_HandleTypeDef *hcan)
{
	if(hcan == interfaceHandler->hcan)
	{
		interfaceMessage_t message;

		message.type = RECEIVED;

		HAL_CAN_GetRxMessage(interfaceHandler->hcan, CAN_RX_FIFO0, &message.RxHeader, message.RxData);

		xQueueSendFromISR(interfaceHandler->queue, ( void * ) &message, NULL);
	}
}



system_t interfaceDriverSendQuery(interfaceHandler_t *handler, interfaceMessageId_t id, uint8_t *buffer, uint8_t size)
{
	interfaceMessage_t message;

	message.type = TO_SEND;
	message.id = id;

	if(size != 0 && buffer != NULL)
	{
		message.size = size;
		memcpy(message.buffer, buffer, size);
	}
	else
	{
		message.size = 0;
	}

	xQueueSend(handler->queue, ( void * ) &message, 0);

	return SYSTEM_OK;
}
