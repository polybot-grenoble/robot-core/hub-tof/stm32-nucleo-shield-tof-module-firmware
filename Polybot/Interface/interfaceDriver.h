/**
* @file interfaceDriver.h
* @author Charles Blanchard
* @brief
* @version 0.2
* @date 30/11/2020
*
*/

#ifndef __INTERFACE_DRIVER_H__
#define __INTERFACE_DRIVER_H__


/* Includes */

#include "main.h"
#include "cmsis_os.h"

#include "systemAPI.h"
#include "interfaceMessageId.h"


/* Defines */

#define RECEIVED	0
#define TO_SEND 	1


/* Typedefs */

/**
* @struct interfaceMessage_t
* @brief Represent a message
*
*/
typedef struct
{
	uint8_t type;	/**< Type of the message. Can be RECEIVED or TO_SEND */

	union
	{
		// interfaceMessage of type RECEIVED - Contains the received buffer and can properties
		struct
		{
			CAN_RxHeaderTypeDef RxHeader;
			uint8_t RxData[8];
		};

		// interfaceMessage of type TO_SEND - Contains the buffer, its size and id message to send
		struct
		{
			uint32_t id;
			uint8_t buffer[8];
			uint8_t size;
		};
	};
} interfaceMessage_t;


typedef system_t (*interfaceDriverCallback_t)(interfaceMessageId_t, uint8_t *, uint8_t);


typedef struct
{
	CAN_HandleTypeDef *hcan;
	QueueHandle_t queue;
	interfaceDriverCallback_t *callback;
} interfaceHandler_t;



/* Prototypes */


system_t interfaceDriverInit(interfaceHandler_t *handler);

system_t interfaceDriverSendQuery(interfaceHandler_t *handler, interfaceMessageId_t id, uint8_t *buffer, uint8_t size);

system_t interfaceDriverRegisterCallback(interfaceHandler_t *handler, interfaceMessageId_t id, interfaceDriverCallback_t callback);
system_t interfaceDriverUnregisterCallback(interfaceHandler_t *handler, interfaceMessageId_t id);

system_t interfaceDriverWaitForMessage(interfaceHandler_t *handler, interfaceMessage_t *message);

system_t interfaceDriverProcess(interfaceHandler_t *handler, CAN_RxHeaderTypeDef RxHeader, uint8_t *RxData);
system_t interfaceDriverSend(interfaceHandler_t *handler, interfaceMessageId_t id, uint8_t *buffer, uint8_t size);

#endif /* __INTERFACE_DRIVER_H */
