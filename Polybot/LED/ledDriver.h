/**
* @file ledDriver.h
* @author Charles Blanchard
* @brief
* @version 0.1
* @date 27/09/2020
*
*/

#ifndef __LED_DRIVER_H
#define __LED_DRIVER_H

/* Includes */

#include "main.h"
#include "cmsis_os.h"

/* Prototypes */

void ledDriverTurnOn();
void ledDriverTurnOff();


#endif /* __LED_DRIVER_H__ */
