/**
* @file ledDriver.c
* @author Charles Blanchard
* @brief
*
* @version 0.1
* @date 27/09/2020
*
*/

/* Includes */

#include "ledDriver.h"


/* Functions */

void ledDriverTurnOn()
{
	HAL_GPIO_WritePin(LED_GPIO_Port, LED_Pin, GPIO_PIN_SET);
}

void ledDriverTurnOff()
{
	HAL_GPIO_WritePin(LED_GPIO_Port, LED_Pin, GPIO_PIN_RESET);
}
