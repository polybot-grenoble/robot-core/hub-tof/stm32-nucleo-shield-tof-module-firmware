/**
* @file ledTask.c
* @author Charles Blanchard
* @brief
* @version 0.1
* @date 27/09/2020
*
*/

/* Includes */

#include "ledDriver.h"


/* Task */

void startLedTask(void const * argument)
{
  for(;;)
  {
    ledDriverTurnOn();
    osDelay(100);
    ledDriverTurnOff();
    osDelay(900);
  }
}
